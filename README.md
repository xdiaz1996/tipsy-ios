# Tipsy Trials iOS

Created By:

	Xavier Diaz (Product Owner)
	Kevin Loi
	Cori Calabi
	Richard Franklin
	Douglas Korody


iOS Application that tests user's cognitive skills while under the influence.

The purpose of this project is a way to let users know if they are drunk, and help them call a rideshare service for their own and others' safety
