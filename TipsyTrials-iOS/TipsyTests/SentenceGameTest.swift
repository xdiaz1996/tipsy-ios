//
//  SentenceGameTest.swift
//  TipsyTests
//
//  Created by Doug Korody on 6/4/18.
//  Copyright © 2018 Tipsy Trials. All rights reserved.
//

import XCTest

class SentenceGameTest: XCTestCase
{
    var sentenceGameCont: SentenceViewController!
    
    override func setUp()
    {
        let testBundle = Bundle(for: type(of: self))
        let storyboard = UIStoryboard(name: "Main", bundle: testBundle)
        let vc = storyboard.instantiateViewController(withIdentifier: "SentenceGameID") as! SentenceViewController
        sentenceGameCont = vc
        _ = sentenceGameCont.view
        sentenceGameCont.counter = 0
        super.setUp()
    }
    
    override func tearDown()
    {
        super.tearDown()
    }
    
    // test the starting sentence in the game
    func testStartingSentenceShouldPass()
    {
        sentenceGameCont.userInputBox.text = "I want to be safe and sober!"
        sentenceGameCont.submitButton.sendActions(for: .touchUpInside)
        XCTAssertEqual(sentenceGameCont.sentenceResult.text,"Sentence correctly typed.")
        
    }
    
    // test case sensitivity, what happens when the user doesnt type anything,
    // and what happens when the user doesnt use the correct punctuation
    func testStartingSentenceShouldFail()
    {
        
        sentenceGameCont.userInputBox.text = "i want to be safe and sober!"
        sentenceGameCont.submitButton.sendActions(for: .touchUpInside)
        XCTAssertNotEqual(sentenceGameCont.sentenceResult.text,"Sentence correctly typed.")
        sentenceGameCont.userInputBox.text = ""
        sentenceGameCont.submitButton.sendActions(for: .touchUpInside)
        XCTAssertNotEqual(sentenceGameCont.sentenceResult.text,"Sentence correctly typed.")
        sentenceGameCont.userInputBox.text = "I want to be safe and sober."
        sentenceGameCont.submitButton.sendActions(for: .touchUpInside)
        XCTAssertNotEqual(sentenceGameCont.sentenceResult.text,"Sentence correctly typed.")
        
    }
    
    //test a random sentence using getRandomSentence()
    func testRandomSentenceShouldPass()
    {
        let temp = sentenceGameCont.sentenceMaker.getRandomSentence()
        sentenceGameCont.textDisplay.text = temp
        sentenceGameCont.userInputBox.text = temp
        sentenceGameCont.submitButton.sendActions(for: .touchUpInside)
        XCTAssertEqual(sentenceGameCont.sentenceResult.text,"Sentence correctly typed.")
        
    }
    
    // see what happens when the user does not capitolize the first letter
    // see what happens when the user leaves the text box blank
    // see what happens when you add one extra character
    func testRandomSentenceShouldFail()
    {
        let temp = sentenceGameCont.sentenceMaker.getRandomSentence()
        sentenceGameCont.textDisplay.text = temp
        sentenceGameCont.userInputBox.text = "i want to be safe and sober!"
        sentenceGameCont.submitButton.sendActions(for: .touchUpInside)
        XCTAssertNotEqual(sentenceGameCont.sentenceResult.text,"Sentence correctly typed.")
        sentenceGameCont.userInputBox.text = ""
        sentenceGameCont.submitButton.sendActions(for: .touchUpInside)
        XCTAssertNotEqual(sentenceGameCont.sentenceResult.text,"Sentence correctly typed.")
        sentenceGameCont.userInputBox.text = temp + "!"
        sentenceGameCont.submitButton.sendActions(for: .touchUpInside)
        XCTAssertNotEqual(sentenceGameCont.sentenceResult.text,"Sentence correctly typed.")        
    }

}
