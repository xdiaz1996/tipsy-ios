//
//  ColorGameTest.swift
//  TipsyTests
//
//  Created by Doug Korody on 6/4/18.
//  Copyright © 2018 Tipsy Trials. All rights reserved.
//

import XCTest

class ColorGameTest: XCTestCase
{
    
    var colorGameCont: ColorMatch!
    
    override func setUp()
    {
        let testBundle = Bundle(for: type(of: self))
        let storyboard = UIStoryboard(name: "Main", bundle: testBundle)
        let vc = storyboard.instantiateViewController(withIdentifier: "ColorGameID") as! ColorMatch
        colorGameCont = vc
        _ = colorGameCont.view
        colorGameCont.counter = 0
        rem = 15
        super.setUp()
    }
    
    override func tearDown()
    {
        super.tearDown()
    }
    
    // fuction that tests the case when the text matches the button color that the simulated user presses
    // for the blue button
    func testBlueButtonShouldPass()
    {
        colorGameCont.prompt.text = "Blue"
        colorGameCont.blueButton.sendActions(for: .touchUpInside)
        XCTAssertEqual(rem, 14)
    }
    
    // fuction that tests the case when the text does not match the button color that the simulated user presses
    // for the blue button
    func testBlueButtonShouldFail()
    {
        colorGameCont.prompt.text = "Red"
        colorGameCont.blueButton.sendActions(for: .touchUpInside)
        XCTAssertEqual(rem, 15)
    }
    
    // fuction that tests the case when the text matches the button color that the simulated user presses
    // for the red button
    func testRedButtonShouldPass()
    {
        colorGameCont.prompt.text = "Red"
        colorGameCont.redButton.sendActions(for: .touchUpInside)
        XCTAssertEqual(rem, 14)
    }
    
    // fuction that tests the case when the text does not match the button color that the simulated user presses
    // for the red button
    func testRedButtonShouldFail()
    {
        colorGameCont.prompt.text = "Blue"
        colorGameCont.redButton.sendActions(for: .touchUpInside)
        XCTAssertEqual(rem, 15)

    }
    
    // fuction that tests the case when the text matches the button color that the simulated user presses
    // for the green button
    func testGreenButtonShouldPass()
    {
        colorGameCont.prompt.text = "Green"
        colorGameCont.greenButton.sendActions(for: .touchUpInside)
        XCTAssertEqual(rem, 14)
    }
    
    // fuction that tests the case when the text does not match the button color that the simulated user presses
    // for the green button
    func testGreenButtonShouldFail()
    {
        colorGameCont.prompt.text = "Blue"
        colorGameCont.greenButton.sendActions(for: .touchUpInside)
        XCTAssertEqual(rem, 15)
        
    }
    
    // fuction that tests the case when the text matches the button color that the simulated user presses
    // for the purple button
    func testPurpleButtonShouldPass()
    {
        colorGameCont.prompt.text = "Purple"
        colorGameCont.purpleButton.sendActions(for: .touchUpInside)
        XCTAssertEqual(rem, 14)
    }
    
    // fuction that tests the case when the text does not match the button color that the simulated user presses
    // for the purple button
    func testPurpleButtonShouldFail()
    {
        colorGameCont.prompt.text = "Blue"
        colorGameCont.purpleButton.sendActions(for: .touchUpInside)
        XCTAssertEqual(rem, 15)
        
    }
    

    
}
