//
//  MathGameTest.swift
//  TipsyTests
//
//  Created by Doug Korody on 6/4/18.
//  Copyright © 2018 Tipsy Trials. All rights reserved.
//

import XCTest
@testable import Pods_TipsyTrials_iOS

class MathGameTest: XCTestCase
{
    var mathGameCont: MathGameController!
    
    override func setUp()
    {
        let testBundle = Bundle(for: type(of: self))
        let storyboard = UIStoryboard(name: "Main", bundle: testBundle)
        let vc = storyboard.instantiateViewController(withIdentifier: "MathViewControllerID") as! MathGameController
        mathGameCont = vc
        _ = mathGameCont.view
        mathGameCont.counter = 0
        mathGameCont.correctCount = 0
        super.setUp()        
    }
    
    override func tearDown()
    {
        mathGameCont = nil
        super.tearDown()
    }
    
    // function that tests the addition part of the calculator with correct
    // simulated input. Tests the addition of two even numbers.
    func testCheckAdditionShouldPassEE()
    {
        mathGameCont.numberOne = 2
        mathGameCont.numberTwo = 2
        mathGameCont.numberOp = 0
        mathGameCont.userAnswer = "4"
        mathGameCont.submitButton.sendActions(for: .touchUpInside)
        XCTAssertTrue(mathGameCont.correctCount == 1)
        XCTAssertEqual(mathGameCont.correctCount, 1)
        
    }
    
    // function that tests the addition part of the calculator with incorrect
    // simulated input. Tests the incorrect addition of two even numbers.
    func testCheckAdditionShouldFailEE()
    {
        mathGameCont.numberOne = 2
        mathGameCont.numberTwo = 2
        mathGameCont.numberOp = 0
        mathGameCont.userAnswer = "3"
        mathGameCont.submitButton.sendActions(for: .touchUpInside)
        XCTAssertTrue(mathGameCont.correctCount == 0)
        XCTAssertEqual(mathGameCont.correctCount, 0)
    }
    
    // function that tests the addition part of the calculator with correct
    // simulated input. Tests the addition of an even and an odd number.
    func testCheckAdditionShouldPassEO()
    {
        mathGameCont.numberOne = 1
        mathGameCont.numberTwo = 2
        mathGameCont.numberOp = 0
        mathGameCont.userAnswer = "3"
        mathGameCont.submitButton.sendActions(for: .touchUpInside)
        XCTAssertTrue(mathGameCont.correctCount == 1)
        XCTAssertEqual(mathGameCont.correctCount, 1)
        
    }
    
    // function that tests the addition part of the calculator with incorrect
    // simulated input. Tests the incorrect addition of an even and an odd number.
    func testCheckAdditionShouldFailEO()
    {
        mathGameCont.numberOne = 1
        mathGameCont.numberTwo = 2
        mathGameCont.numberOp = 0
        mathGameCont.userAnswer = "4"
        mathGameCont.submitButton.sendActions(for: .touchUpInside)
        XCTAssertTrue(mathGameCont.correctCount == 0)
        XCTAssertEqual(mathGameCont.correctCount, 0)
    }
    
    // function that tests the addition part of the calculator with correct
    // simulated input. Tests the addition of two odd numbers.
    func testCheckAdditionShouldPassOO()
    {
        mathGameCont.numberOne = 1
        mathGameCont.numberTwo = 3
        mathGameCont.numberOp = 0
        mathGameCont.userAnswer = "4"
        mathGameCont.submitButton.sendActions(for: .touchUpInside)
        XCTAssertTrue(mathGameCont.correctCount == 1)
        XCTAssertEqual(mathGameCont.correctCount, 1)
        
    }
    
    // function that tests the addition part of the calculator with incorrect
    // simulated input. Tests the incorrect addition of two odd numbers.
    func testCheckAdditionShouldFailOO()
    {
        mathGameCont.numberOne = 1
        mathGameCont.numberTwo = 3
        mathGameCont.numberOp = 0
        mathGameCont.userAnswer = "5"
        mathGameCont.submitButton.sendActions(for: .touchUpInside)
        XCTAssertTrue(mathGameCont.correctCount == 0)
        XCTAssertEqual(mathGameCont.correctCount, 0)
    }
    
    // function that tests the subtraction part of the calculator with correct
    // simulated input. Tests the subtraction of two even numbers.
    func testCheckSubShouldPassEE()
    {
        mathGameCont.numberOp = 1
        mathGameCont.numberOne = 4
        mathGameCont.numberTwo = 2
        mathGameCont.userAnswer = "2"
        mathGameCont.submitButton.sendActions(for: .touchUpInside)
        XCTAssertTrue(mathGameCont.correctCount == 1)
        XCTAssertEqual(mathGameCont.correctCount, 1)
        
    }
    
    // function that tests the subtraction part of the calculator with incorrect
    // simulated input. Tests the incorrect subtraction of two even numbers.
    func testCheckSubShouldFailEE()
    {
        mathGameCont.numberOp = 1
        mathGameCont.numberOne = 4
        mathGameCont.numberTwo = 2
        mathGameCont.userAnswer = "1"
        mathGameCont.submitButton.sendActions(for: .touchUpInside)
        XCTAssertTrue(mathGameCont.correctCount == 0)
        XCTAssertEqual(mathGameCont.correctCount, 0)
        
    }
    
    // function that tests the subtraction part of the calculator with correct
    // simulated input. Tests the subtraction of an even and a odd number.
    func testCheckSubShouldPassEO()
    {
        mathGameCont.numberOp = 1
        mathGameCont.numberOne = 4
        mathGameCont.numberTwo = 1
        mathGameCont.userAnswer = "3"
        mathGameCont.submitButton.sendActions(for: .touchUpInside)
        XCTAssertTrue(mathGameCont.correctCount == 1)
        XCTAssertEqual(mathGameCont.correctCount, 1)
        
    }
    
    // function that tests the subtraction part of the calculator with incorrect
    // simulated input. Tests the incorrect subtraction of an even and a odd number.
    func testCheckSubShouldFailEO()
    {
        mathGameCont.numberOp = 1
        mathGameCont.numberOne = 4
        mathGameCont.numberTwo = 1
        mathGameCont.userAnswer = "1"
        mathGameCont.submitButton.sendActions(for: .touchUpInside)
        XCTAssertTrue(mathGameCont.correctCount == 0)
        XCTAssertEqual(mathGameCont.correctCount, 0)
        
    }
    
    // function that tests the subtraction part of the calculator with correct
    // simulated input. Tests the subtraction of two odd numbers.
    func testCheckSubShouldPassOO()
    {
        mathGameCont.numberOp = 1
        mathGameCont.numberOne = 3
        mathGameCont.numberTwo = 1
        mathGameCont.userAnswer = "2"
        mathGameCont.submitButton.sendActions(for: .touchUpInside)
        XCTAssertTrue(mathGameCont.correctCount == 1)
        XCTAssertEqual(mathGameCont.correctCount, 1)
        
    }
    
    // function that tests the subtraction part of the calculator with incorrect
    // simulated input. Tests the incorrect subtraction of two odd numbers.
    func testCheckSubShouldFailOO()
    {
        mathGameCont.numberOp = 1
        mathGameCont.numberOne = 3
        mathGameCont.numberTwo = 1
        mathGameCont.userAnswer = "1"
        mathGameCont.submitButton.sendActions(for: .touchUpInside)
        XCTAssertTrue(mathGameCont.correctCount == 0)
        XCTAssertEqual(mathGameCont.correctCount, 0)
        
    }
    
    // function that tests the multiplication part of the calculator with correct
    // simulated input. Tests the multiplication of two even numbers.
    func testCheckMultShouldPassEE()
    {
        mathGameCont.numberOp = 2
        mathGameCont.numberOne = 4
        mathGameCont.numberTwo = 4
        mathGameCont.userAnswer = "16"
        mathGameCont.submitButton.sendActions(for: .touchUpInside)
        XCTAssertTrue(mathGameCont.correctCount == 1)
        XCTAssertEqual(mathGameCont.correctCount, 1)
        
    }
    
    // function that tests the multiplication part of the calculator with incorrect
    // simulated input. Tests the incorrect multiplication of two even numbers.
    func testCheckMultShouldFailEE()
    {
        mathGameCont.numberOp = 2
        mathGameCont.numberOne = 4
        mathGameCont.numberTwo = 4
        mathGameCont.userAnswer = "6"
        mathGameCont.submitButton.sendActions(for: .touchUpInside)
        XCTAssertTrue(mathGameCont.correctCount == 0)
        XCTAssertEqual(mathGameCont.correctCount, 0)
        
    }
    
    // function that tests the multiplication part of the calculator with correct
    // simulated input. Tests the correct multiplication of an even and a odd number.
    func testCheckMultShouldPassEO()
    {
        mathGameCont.numberOp = 2
        mathGameCont.numberOne = 4
        mathGameCont.numberTwo = 3
        mathGameCont.userAnswer = "12"
        mathGameCont.submitButton.sendActions(for: .touchUpInside)
        XCTAssertTrue(mathGameCont.correctCount == 1)
        XCTAssertEqual(mathGameCont.correctCount, 1)
        
    }
    
    // function that tests the multiplication part of the calculator with inccorrect
    // simulated input. Tests the incorrect multiplication of an even and a odd number.
    func testCheckMultShouldFailEO()
    {
        mathGameCont.numberOp = 2
        mathGameCont.numberOne = 4
        mathGameCont.numberTwo = 3
        mathGameCont.userAnswer = "6"
        mathGameCont.submitButton.sendActions(for: .touchUpInside)
        XCTAssertTrue(mathGameCont.correctCount == 0)
        XCTAssertEqual(mathGameCont.correctCount, 0)
        
    }
    
    // function that tests the multiplication part of the calculator with correct
    // simulated input. Tests the multiplication of two odd numbers.
    func testCheckMultShouldPassOO()
    {
        mathGameCont.numberOp = 2
        mathGameCont.numberOne = 3
        mathGameCont.numberTwo = 3
        mathGameCont.userAnswer = "9"
        mathGameCont.submitButton.sendActions(for: .touchUpInside)
        XCTAssertTrue(mathGameCont.correctCount == 1)
        XCTAssertEqual(mathGameCont.correctCount, 1)
        
    }
    
    // function that tests the multiplication part of the calculator with incorrect
    // simulated input. Tests the incorrect multiplication of two odd numbers.
    func testCheckMultShouldFailOO()
    {
        mathGameCont.numberOp = 2
        mathGameCont.numberOne = 3
        mathGameCont.numberTwo = 3
        mathGameCont.userAnswer = "6"
        mathGameCont.submitButton.sendActions(for: .touchUpInside)
        XCTAssertTrue(mathGameCont.correctCount == 0)
        XCTAssertEqual(mathGameCont.correctCount, 0)
        
    }
    
    // function that tests the division part of the calculator with correct
    // simulated input. Tests the division of two even numbers.
    func testCheckDivShouldPassEE()
    {
        mathGameCont.numberOp = 3
        mathGameCont.numberOne = 8
        mathGameCont.numberTwo = 2
        mathGameCont.userAnswer = "4"
        mathGameCont.submitButton.sendActions(for: .touchUpInside)
        XCTAssertTrue(mathGameCont.correctCount == 1)
        XCTAssertEqual(mathGameCont.correctCount, 1)
        
    }
    
    // function that tests the division part of the calculator with incorrect
    // simulated input. Tests the incorrect divison of two even numbers.
    func testCheckDivShouldFailEE()
    {
        mathGameCont.numberOp = 3
        mathGameCont.numberOne = 8
        mathGameCont.numberTwo = 2
        mathGameCont.userAnswer = "5"
        mathGameCont.submitButton.sendActions(for: .touchUpInside)
        XCTAssertTrue(mathGameCont.correctCount == 0)
        XCTAssertEqual(mathGameCont.correctCount, 0)
        
    }
    
    // function that tests the division part of the calculator with correct
    // simulated input. Tests the division of two odd numbers.
    func testCheckDivShouldPassOO()
    {
        mathGameCont.numberOp = 3
        mathGameCont.numberOne = 9
        mathGameCont.numberTwo = 3
        mathGameCont.userAnswer = "3"
        mathGameCont.submitButton.sendActions(for: .touchUpInside)
        XCTAssertTrue(mathGameCont.correctCount == 1)
        XCTAssertEqual(mathGameCont.correctCount, 1)
        
    }
    
    // function that tests the division part of the calculator with incorrect
    // simulated input. Tests the incorrect division of two odd numbers.
    func testCheckDivShouldFailOO()
    {
        mathGameCont.numberOp = 3
        mathGameCont.numberOne = 9
        mathGameCont.numberTwo = 3
        mathGameCont.userAnswer = "5"
        mathGameCont.submitButton.sendActions(for: .touchUpInside)
        XCTAssertTrue(mathGameCont.correctCount == 0)
        XCTAssertEqual(mathGameCont.correctCount, 0)
        
    }
    
}
