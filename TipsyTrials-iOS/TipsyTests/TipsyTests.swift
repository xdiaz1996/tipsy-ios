//
//  TipsyTests.swift
//  TipsyTests
//
//  Created by Doug Korody on 6/4/18.
//  Copyright © 2018 Tipsy Trials. All rights reserved.
//

import XCTest

class TipsyTests: XCTestCase
{
    let sentenceMaker = SentenceMaker()
    
    override func setUp()
    {
        super.setUp()
    }
    
    override func tearDown()
    {
       
    }
    
    //test the random sentence generator
    func testSentence()
    {
        XCTAssertNotNil(sentenceMaker.getRandomSentence())
    }
    
    //test how fast the sentence was generated
    func testSentencePerformance()
    {
        self.measure
        {
            sentenceMaker.getRandomSentence()
        }
    }
    
}
