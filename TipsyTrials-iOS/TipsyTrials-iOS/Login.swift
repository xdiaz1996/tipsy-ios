//
//  Login.swift
//  TipsyTrials-iOS
//
//  Created by Vanya Nanda on 5/25/18.
//  Copyright © 2018 Tipsy Trials. All rights reserved.
//

import UIKit
import FirebaseAuth

class Login: UIViewController
{

    @IBOutlet weak var loginSelector: UISegmentedControl!
    @IBOutlet weak var loginLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var passwordLbl: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var errorLbl: UILabel!
    
    var didLogin:Bool = false
    var isLogin:Bool = true
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.errorLbl.text = " "
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //this function changes user interaction if login or register is selected
    @IBAction func loginSelectorChanged(_ sender: UISegmentedControl)
    {
        isLogin = !isLogin
        if isLogin
        {
            loginLbl.text = "Sign In"
            loginBtn.setTitle("Sign In", for: .normal)
        }
        else
        {
            loginLbl.text = "Register"
            loginBtn.setTitle("Register", for: .normal)
        }
    }
    
    // will either login, register or throw appropriate errors
    @IBAction func loginPresses(_ sender: UIButton)
    {
        
        if let emailCheck = emailTextField.text, let passCheck = passwordTextField.text
        {
            let email = emailTextField.text!
            let password = passwordTextField.text!
            if isLogin
            {
                Auth.auth().signIn(withEmail: email, password: password)
                {
                    (user, error) in
                    if let u = user
                    {
                        self.performSegue(withIdentifier: "goHome", sender: self)
                    }
                    else
                    {
                        self.errorLbl.text = "Incorrect Login info or account doesn't exist"

                    }
                }
            }
            else
            {
                if password.count < 6
                {
                    self.errorLbl.text = "password not 6 characters or longer"
                }
                else
                {
                    Auth.auth().createUser(withEmail: email, password: password) { (authResult, error) in
                        if let res = authResult
                        {
                            self.performSegue(withIdentifier: "goHome", sender: self)
                            
                        }
                        else
                        {
                            self.errorLbl.text = "Email already taken or invalid"
                        }   }
                }
            }
        }
    }
    
    // This closes the keyboard when you click off of it
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
    }
}
