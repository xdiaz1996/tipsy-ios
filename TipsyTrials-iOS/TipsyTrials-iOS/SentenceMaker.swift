//
//  SentenceMaker.swift
//  Sentence
//  This class contains a sentence maker that returns a random sentence
//  Created by Doug Korody on 5/2/18.
//  Copyright © 2018 Doug Korody. All rights reserved.
//

import Foundation

class SentenceMaker
{
    
    var indexNoun1: uint = 0
    var indexNoun2: uint = 0
    var indexAdj1: uint = 0
    var indexAdj2: uint = 0
    var indexVerb: uint = 0
    let adjList1: [String] = ["bald","beautiful","chubby",
                              "clean","dazzling","drab","elegant","fancy",
                              "fit","flabby","glamorous","gorgeous","handsome",
                              "long","muscular","plain","plump",
                              "quaint","scruffy","shapely","short","skinny",
                              "stocky","ugly","unkempt",
                              "unsightly"]
    
    let adjList2: [String] = ["angrily","clumsily","fiercely","grumpily",
                              "itchily","jealously","lazily","nervously",
                              "panicky","pitifully","scarily","uptightly",
                              "worrily","agreeably","bravely","calmly",
                              "eagerly","faithfuly","gently","happily",
                              "jollily","kindly","lively","nicely",
                              "politely","proudly","sillily",
                              "thankfuly","wittily","zealousy"]
    
    let nounList: [String] = ["time", "year","people","way","day","man","thing",
                              "woman","life","child","world","school","state",
                              "family","student","group","country","problem",
                              "hand","part","place","case","week","company",
                              "system","program","question","work",
                              "number","night","point","home","water","room",
                              "mother","area","money","story","fact","month",
                              "lot","right","study","book","eye","job","word",
                              "business","issue","dog"]
    
    let verbList: [String] = ["said","made","went","took","came","saw","knew",
                              "got","gave","found","thought","told","became",
                              "showed","left","felt","put","brought","began",
                              "kept","held","wrote","stood","heard","let",
                              "meant","set","met","ran","paid","sat","spoke",
                              "lay","led","read","grew","lost","fell","sent",
                              "built","drew","broke","spent","cut",
                              "rose","drove","bought","wore",
                              "chose"]
    
    //This function returns a random sentence made from the arrays of words
    //above It uses the arc4random_uniform function to return a random unsigned
    //int which is used to access random words in the lists above
    func getRandomSentence()->String
    {
        var tempSentence:String
        indexAdj1 = arc4random_uniform(UInt32(adjList1.count)-1)
        indexAdj2 = arc4random_uniform(UInt32(adjList2.count)-1)
        indexNoun1 = arc4random_uniform(UInt32(nounList.count)-1)
        indexNoun2 = arc4random_uniform(UInt32(nounList.count)-1)
        indexVerb = arc4random_uniform(UInt32(verbList.count)-1)
        
        //I had to split up the sentence concatination into multiple lines
        //because of the 80 character line limit
        tempSentence = "The " + adjList1[Int(indexAdj1)] + " "
        tempSentence += nounList[Int(indexNoun1)] + " "
        tempSentence += adjList2[Int(indexAdj2)] + " "
        tempSentence += verbList[Int(indexVerb)]
        tempSentence += " the " + nounList[Int(indexNoun2)] + "."
        return tempSentence
    }
}
