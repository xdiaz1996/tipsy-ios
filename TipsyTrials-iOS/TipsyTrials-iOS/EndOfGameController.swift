//
//  EndOfGameController.swift
//  TipsyTrials-iOS
//
//  Created by Cori Calabi on 5/21/18.
//  Copyright © 2018 Tipsy Trials. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class EndOfGameController: UIViewController
{
    @IBOutlet var endTime: UILabel!
    @IBOutlet weak var drunkOrNotText: UILabel!
    @IBOutlet weak var getRideText: UILabel!
    @IBOutlet var uberBtn: UIButton!
    @IBOutlet var lyftBtn: UIButton!
    
    var counter:Double!
    var isTestCase:Bool!
    var baseScore:String!
    // database stuff
    var ref: DatabaseReference!
    var databaseHandle: DatabaseHandle!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.endTime.text = "Time: " + String(format: "%.1f", self.counter)
        self.ref = Database.database().reference()
        
        // checks for test case or setting baseline
        if (isTestCase)
        {
            // displays output for drunk testing
            self.drunkOrNot()
        }
        else
        {
            // displays out put for setting baseline
            self.storeBaseline()
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // outputs test case for whether user is drunk or not
    func drunkOrNot()
    {
        // make sure uber and lyft buttons are not hidden
        uberBtn.isHidden = false
        lyftBtn.isHidden = false
        
        let user = Auth.auth().currentUser
        if (user != nil)
        {
            let userID = user!.uid
            ref.child("users").child(userID).observe(.value, with: {(snapshot) in
                if let dictionary = snapshot.value as? NSDictionary
                {
                    if let score = dictionary["score"] as? String
                    {
                        // option 1: if baseline is set, compare to baseline
                        self.baseScore = score
                        
                        let baseScoreInt = (self.baseScore as NSString).doubleValue
                        let baseScore = baseScoreInt * 1.25

                        if (self.counter >= baseScore)
                        {
                            self.drunkOrNotText.text = "By our estimates, you are drunk!"
                            self.getRideText.text = "You should get a ride:"
                        }
                        else
                        {
                            self.drunkOrNotText.text = "By our estimates, you are not drunk."
                            self.getRideText.text = "You might still want to get a ride:"
                        }
                    }
                    else
                    {
                        // option 2: if baseline is not set, compare to 120
                        self.displayDefaultResults()
                    }
                }
                else
                {
                    self.displayDefaultResults()
                }
            })
        }
        else
        {
            self.displayDefaultResults()
        }
    }
    
    // displays default results
    // aka when baseline is not set or if user is not logged in
    func displayDefaultResults()
    {
        if (self.counter >= 120)
        {
            self.drunkOrNotText.text = "By our estimates, you are drunk!"
            self.getRideText.text = "You should get a ride:"
        }
        else
        {
            self.drunkOrNotText.text = "By our estimates, you are not drunk."
            self.getRideText.text = "You might still want to get a ride:"
        }
    }
    
    // Only call this function if user is authenticated
    func storeBaseline()
    {
        // hide uber and lyft buttons
        uberBtn.isHidden = true
        lyftBtn.isHidden = true
        
        // display time
        endTime.text = String(format: "%.1f", self.counter)
        
        // store in database
        let score = endTime.text!
        let userID = Auth.auth().currentUser!.uid
        self.ref.child("users").child(userID).updateChildValues(["score": score])
        
        // displays after written to database.
        drunkOrNotText.text = "Your base time has been set!"
    }

    // takes user to uber web link
    @IBAction func openUber(_ sender: Any)
    {
        let uberWebl = URL(string: "https://m.uber.com/?action=setPickup")!
        
        if UIApplication.shared.canOpenURL(uberWebl)
        {
            UIApplication.shared.open(uberWebl)
        }
        else
        {
            print("no app found")
        }
    }
    
    // takes user to lyft web link
    @IBAction func openLyft(_ sender: Any)
    {
        let lyftWebl = URL(string: "https://lyft.com/ride?id=lyft")!
        
        if UIApplication.shared.canOpenURL(lyftWebl)
        {
            UIApplication.shared.open(lyftWebl)
        }
        else
        {
            print("no app found")
        }
    }
}
