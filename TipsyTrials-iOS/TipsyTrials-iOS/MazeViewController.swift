/*
    Copyright (C) 2016 Apple Inc. All Rights Reserved.
    See LICENSE.txt for this sample’s licensing information
    
    Abstract:
    A `UIViewController` subclass that stores references to game-wide input sources and managers.
*/

import UIKit
import SpriteKit

class MazeViewController: UIViewController
{
    // MARK: Properties
    let scene = GameScene(fileNamed: "GameScene")!
    var counter:Double!
    var timer = Timer()
    var isTestCase:Bool?
    
    @IBOutlet var titleLabel: UILabel!

    // MARK: Methods
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let skView = view as! SKView
        skView.isUserInteractionEnabled = true
        
        // Set the scale mode to scale to fit the window.
        scene.scaleMode = .aspectFit
        
        skView.presentScene(scene)
        
        // SpriteKit applies additional optimizations to improve rendering performance.
        skView.ignoresSiblingOrder = true
        
        timer = Timer.scheduledTimer(timeInterval: 0.1, target: self,
                                     selector: #selector(UpdateTime),
                                     userInfo: nil, repeats: true)
    }
    
    @objc func UpdateTime()
    {
        counter = counter + 0.1
        titleLabel.text = "Time: " + String(format: "%.1f", counter)
    }
    
    /**
        Detects taps. If a tap is detected, the the game
        advances by creating a new maze or solving the existing maze.
    */
    @IBAction func handleTap(_ gestureRecognizer: UITapGestureRecognizer)
    {
        let tapLocation = gestureRecognizer.location(in: view)
        let viewSize = view.frame.size

        let tapFromCenter = int2(Int32(tapLocation.x - (viewSize.width/2)),
                                 Int32(tapLocation.y - (viewSize.height/2)))
        
        var finished : Bool
        
        if (abs(tapFromCenter.x) >= abs(tapFromCenter.y))
        {
           // Choose left/right direction
            if (tapFromCenter.x >= 0)
            {
                finished = scene.movePlayer(direction: MazeBuilder.Direction.right)
            }
            else
            {
                finished = scene.movePlayer(direction: MazeBuilder.Direction.left)
            }
        }
        else
        {
            if (tapFromCenter.y >= 0)
            {
                finished = scene.movePlayer(direction: MazeBuilder.Direction.down)
            }
            else
            {
                finished = scene.movePlayer(direction: MazeBuilder.Direction.up)
            }
        }
        
        if (finished)
        {
            timer.invalidate()
            // Game is done
            performSegue(withIdentifier: "EndOfGameSegue", sender: self)
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let endOfGameViewController = segue.destination as? EndOfGameController
        {
            endOfGameViewController.counter = counter
            endOfGameViewController.isTestCase = isTestCase
        }
    }
}
