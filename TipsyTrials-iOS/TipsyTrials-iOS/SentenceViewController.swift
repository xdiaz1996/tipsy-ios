//
//  SentenceViewController.swift
//  TipsyTrials-iOS
//
//  Created by Doug Korody on 5/5/18.
//  Copyright © 2018 Tipsy Trials. All rights reserved.
//

import UIKit

class SentenceViewController: UIViewController,UITextFieldDelegate
{
    
    @IBOutlet var titlelabel: UILabel!
    @IBOutlet weak var sentenceResult: UILabel!
    @IBOutlet weak var sentenceTitle: UILabel!
    @IBOutlet weak var sentenceInst: UILabel!
    @IBOutlet weak var sentenceInst2: UILabel!
    @IBOutlet weak var textDisplay: UILabel!
    @IBOutlet weak var userInputBox: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var scoreText: UILabel!
    
    var counter:Double!
    var timer = Timer()
    let sentenceMaker = SentenceMaker()
    var score: Int = 0
    var scoreString: String = "0"
    var isTestCase:Bool?
    
    
    override func viewDidLoad()
    {
        timer = Timer.scheduledTimer(timeInterval: 0.1, target: self,
                                     selector: #selector(UpdateTime),
                                     userInfo: nil, repeats: true)
        score = 0
        scoreString = "0"
        userInputBox.delegate = self
        sentenceResult.isHidden = true
        submitButton.isEnabled = true
        textDisplay.text = "I want to be safe and sober!"
        userInputBox.spellCheckingType = UITextSpellCheckingType.no
        userInputBox.autocorrectionType = UITextAutocorrectionType.no
        userInputBox.autocapitalizationType =
            UITextAutocapitalizationType.sentences
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func UpdateTime()
    {
        counter = counter + 0.1
        titlelabel.text = "Time: " + String(format: "%.1f", counter)
    }
    
    //link up the return button to the submit button
    //and get rid of the keyboard when return button is pressed
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        self.submitPressed(submitButton)
        textField.resignFirstResponder()
        return true
    }
    
    // hide the keyboard if the user taps on the screen
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        self.view.endEditing(true)
    }
    
    //function that gets called when the submit button is pressed
    //It checks to see if the user spelled the sentence correctly
    //updates the score if the user is correct and gets a new
    //random sentence from sentence maker
    //after the third correct answer it hides all of the layout components
    @IBAction func submitPressed(_ sender: Any)
    {
        userInputBox.resignFirstResponder()
        sentenceResult.isHidden = false
        
        /**
         * CHEAT CODE: REMOVE THIS IF YOU WANT
         */
        if (userInputBox.text == "00000")
        {
            performSegue(withIdentifier: "winSentence", sender: self)
        }
        /**
         * ^^^ CHEAT CODE ABOVE ^^^
         */
        
        if (userInputBox.text == textDisplay.text)
        {
            if (score < 2)
            {
                score += 1
            }
            else
            {
                //stop the timer
                timer.invalidate()
                performSegue(withIdentifier: "winSentence", sender: self)
            }
            userInputBox.text = ""
            sentenceResult.text = "Sentence correctly typed."
            scoreString = String(score)
            textDisplay.text = sentenceMaker.getRandomSentence()
        }
        else
        {
            sentenceResult.text = "Incorrect, please type again!"
        }
        scoreText.text = "Score: " + scoreString
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let colorController = segue.destination as? ColorMatch {
            colorController.counter = counter
            colorController.isTestCase = isTestCase
        }
    }
}
