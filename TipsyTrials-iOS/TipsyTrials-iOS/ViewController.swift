//
//  ViewController.swift
//  TipsyTrials-iOS
//
//  Created by MAC Air on 5/2/18.
//  Copyright © 2018 Tipsy Trials. All rights reserved.
//

import UIKit
import FirebaseAuth

class ViewController: UIViewController
{
    var counter = 0.0
    var isTestCase:Bool! = true
    var isSetBase:Bool!
    
    @IBOutlet var playButton: UIButton!
    @IBOutlet var profileButton:UIButton!
    @IBOutlet var loginButton:UIButton!
   
    override func viewDidLoad()
    {
        super.viewDidLoad()
        playButton.isEnabled = true
        //if the user is already logged in then the login button changes to a log out button
        if Auth.auth().currentUser != nil
        {

            profileButton.isHidden=false
            profileButton.isEnabled = true
            loginButton.setTitle("Log Out", for: .normal)
            
        }
        else
        {
            //User Not logged in
            profileButton.isHidden=true
            profileButton.isEnabled = false
            loginButton.setTitle("Log In", for: .normal)
            
        }
        
    }
    
    //button listener that gets called when the user presses the play button
    //the function performs a segue to start the games
    @IBAction func btnPlay(_ sender: UIButton)
    {
            isTestCase = true
            self.performSegue(withIdentifier: "goSentence", sender: self)
    }
    
    //button listener function that gets called when the user presses the login button
    @IBAction func loginPressed(_ sender: Any)
    {
        let firebaseAuth = Auth.auth()
        if firebaseAuth.currentUser != nil
        {
            
            do
            {
                // if the user presses the log in button while they are already logged in
                // then they will be logged out and the logout button will change to a login button
                try firebaseAuth.signOut()
                loginButton.setTitle("Log In", for: .normal)
                profileButton.isHidden = true
                profileButton.isEnabled = false
            }
            catch let signOutError as NSError
            {
                print ("Error signing out: \(signOutError)")
            }
            catch
            {
                print("Unknown error.")
            }
        }
        else
        {
            //go to log in page
            self.performSegue(withIdentifier: "goLogin", sender: self)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let sentenceController = segue.destination as? SentenceViewController
        {
            sentenceController.counter = counter
            sentenceController.isTestCase = isTestCase
        }
    }

    
}

