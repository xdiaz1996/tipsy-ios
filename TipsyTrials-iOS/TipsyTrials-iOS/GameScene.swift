/*
    Copyright (C) 2016 Apple Inc. All Rights Reserved.
    See LICENSE.txt for this sample’s licensing information
    
    Abstract:
    An `SKScene` subclass that handles logic and visuals.
*/

import SpriteKit
import GameplayKit

class GameScene: SKScene
{
    // MARK: Properties
    
    /// Holds information about the maze.
    var maze = Maze()
    
    
    /**
        Contains optional sprite nodes that are used to visualize the maze 
        graph. The nodes are arranged in a 2D array (an array with rows and 
        columns) so that the array index of a sprite node in this array 
        corresponds to the coordinates of the node in the maze graph. A node at 
        an index exists if the corresponding maze node exists; otherwise, the 
        sprite node is nil.
    */
    @nonobjc var spriteNodes = [[SKSpriteNode?]]()
    
    /**
        Creates a maze object, and creates a visual representation of that maze
        using sprites.
    */
    func createMaze()
    {
        print("Creating maze")
        maze = Maze()
        generateMazeNodes()
    }

    // MARK: SpriteKit Methods
    
    /// Generates a maze when the game starts.
    override func didMove(to _: SKView)
    {
        createMaze()
    }
    
    /// Generates sprite nodes that comprise the maze's visual representation.
    func generateMazeNodes()
    {
        // Initialize the an array of sprites for the maze.
        spriteNodes += [[SKSpriteNode?]](repeating: [SKSpriteNode?](repeating: nil, count: (Maze.dimensions * 2) - 1), count: Maze.dimensions
        )
        
        /*
            Grab the maze's parent node from the scene and use it to
            calculate the size of the maze's cell sprites.
        */
        let mazeParentNode = childNode(withName: "maze") as! SKSpriteNode
        let cellDimension = mazeParentNode.size.height / CGFloat(Maze.dimensions)
        
        // Remove existing maze cell sprites from the previous maze.
        mazeParentNode.removeAllChildren()
        
        // For each maze node in the maze graph, create a corresponding sprite.
        let graphNodes = maze.graph.nodes as? [GKGridGraphNode]
        for node in graphNodes!
        {
            // Get the position of the maze node.
            let x = Int(node.gridPosition.x)
            let y = Int(node.gridPosition.y)
            
            /*
                Create a maze sprite node and place the sprite at the correct 
                location relative to the maze's parent node.
            */
            let mazeNode = SKSpriteNode(
                color: SKColor.white,
                size: CGSize(width: cellDimension, height: cellDimension)
            )
            mazeNode.anchorPoint = CGPoint(x: 0, y: 0)
            mazeNode.position = CGPoint(x: CGFloat(x) * cellDimension, y: CGFloat(y) * cellDimension)
            
            // Add the maze sprite node to the maze's parent node.
            mazeParentNode.addChild(mazeNode)
            
            /*
                Add the maze sprite node to the 2D array of sprite nodes so we 
                can reference it later.
            */
            spriteNodes[x][y] = mazeNode
        }
        
        // Grab the coordinates of the start and end maze sprite nodes.
        let playerNodeX = Int(maze.playerNode.gridPosition.x)
        let playerNodeY = Int(maze.playerNode.gridPosition.y)
        let endNodeX   = Int(maze.endNode.gridPosition.x)
        let endNodeY   = Int(maze.endNode.gridPosition.y)
        
        // Color the start and end nodes green and red, respectively.
        spriteNodes[playerNodeX][playerNodeY]?.color = SKColor.green
        spriteNodes[endNodeX][endNodeY]?.color     = SKColor.red
    }
    
    func movePlayer(direction: MazeBuilder.Direction) -> Bool
    {
        let playerNodeX = maze.playerNode.gridPosition.x
        let playerNodeY = maze.playerNode.gridPosition.y
        var newPosition: int2
        var newNode: GKGridGraphNode?
        
        if(direction == MazeBuilder.Direction.right)
        {
            // Move right
            newPosition = int2(playerNodeX + 1, playerNodeY)
        }
        else if(direction == MazeBuilder.Direction.left)
        {
            // Move left
            newPosition = int2(playerNodeX - 1, playerNodeY)
        }
        else if(direction == MazeBuilder.Direction.down)
        {
            // Move Down
            newPosition = int2(playerNodeX, playerNodeY - 1)
        }
        else
        {
            // Move up
            newPosition = int2(playerNodeX, playerNodeY + 1)
            
        }
        // Set node to new position
        newNode = maze.graph.node(atGridPosition: newPosition)

        if(newNode != nil)
        {
            maze.playerNode = newNode!
            spriteNodes[Int(newPosition.x)][Int(newPosition.y)]?.color = SKColor.green
            
            // Replace Previous Position With White
            spriteNodes[Int(playerNodeX)][Int(playerNodeY)]?.color = SKColor.white
        }
        
        // Check if completed
        if (newPosition.x == Maze.dimensions - 1 && newPosition.y == 0)
        {
            return true
        }
        else
        {
            return false
        }
    }
}
