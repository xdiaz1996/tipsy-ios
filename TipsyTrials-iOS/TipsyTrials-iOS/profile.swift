//
//  profile.swift
//  TipsyTrials-iOS
//
//  Created by MAC Air on 5/30/18.
//  Copyright © 2018 Tipsy Trials. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class profile: UIViewController {
    
    // text fields and base score
    @IBOutlet var buildText: UITextField!
    @IBOutlet var birthdayText: UITextField!
    @IBOutlet var emailText: UITextField!
    @IBOutlet var usernameText: UITextField!
    @IBOutlet var weightText: UITextField!
    @IBOutlet var baseScore: UILabel!
    
    // isSetBase: true if clicked set baseline, false if home
    var isSetBase:Bool!
    
    // database stuff
    var ref: DatabaseReference!
    var databaseHandle: DatabaseHandle!
    
    
    override func viewDidLoad()
    {
//        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // set up firebase reference
        ref = Database.database().reference()
        
        // retrieve values and listen for changes
        let userID = Auth.auth().currentUser?.uid
        ref.child("users").child(userID!).observe(.value, with: {(snapshot) in
            //print(snapshot)
            if let dictionary = snapshot.value as? NSDictionary
            {
                
                if let build = dictionary["build"] as? String
                {
                    // print(build)
                    self.buildText.text = build
                }
                
                if let birthday = dictionary["date_of_birth"] as? String
                {
                    // print(birthday)
                    self.birthdayText.text = birthday
                }
                
                if let email = dictionary["email"] as? String
                {
                    //print(email)
                    self.emailText.text = email
                }
                
                if let score = dictionary["score"] as? String
                {
                    // print(score)
                    self.baseScore.text = score 
                }
                
                if let username = dictionary["username"] as? String
                {
                    // print(username)
                    self.usernameText.text = username
                }
                
                if let weight = dictionary["weight"] as? String
                {
                    // print(weight)
                    self.weightText.text = weight
                }
            }
        })
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // takes user to start of game to set baseline
    @IBAction func setBaseline(_ sender: Any)
    {
        // returns to home page with isTestCase as false
        isSetBase = true
        performSegue(withIdentifier: "toGame", sender: self)
    }
    
    // saves/writes all data to firebase
    @IBAction func saveBtn(_ sender: Any)
    {
        // set variables to text entries
        let build = buildText.text
        let birthday = birthdayText.text
        let email = emailText.text
        let username = usernameText.text
        let weight = weightText.text
        let score = baseScore.text
        
        // write the data to Firebase
        let userID = Auth.auth().currentUser?.uid
        let post = [
            "build":  build,
            "date_of_birth": birthday,
            "email":   email,
            "score": score,
            "username": username,
            "weight": weight
        ]
        self.ref.child("users").child(userID!).setValue(post)
    }
    
    // goes back to the home page
    @IBAction func homeBtn(_ sender: Any)
    {
        // returns to home page with isTestCase as true
        isSetBase = false
        performSegue(withIdentifier: "toHomeSegue", sender: self)
    }
    
    // sets values depending on which view controller it's headed towards
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let home = segue.destination as? ViewController
        {
            home.isSetBase = isSetBase
        }

        if let sentenceController = segue.destination as? SentenceViewController
        {
            sentenceController.counter = 0.0
            sentenceController.isTestCase = false
        }
    }
}
