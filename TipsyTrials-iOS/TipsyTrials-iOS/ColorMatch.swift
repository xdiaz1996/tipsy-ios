//
//  ColorMatch.swift
//  Color Match
//
//  Copyright � 2018 bob. All rights reserved.
//

import UIKit
import Darwin

var rem = 15

class ColorMatch: UIViewController
{
    
    @IBOutlet weak var prompt: UILabel!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var blueButton:UIButton!
    @IBOutlet var redButton:UIButton!
    @IBOutlet var greenButton:UIButton!
    @IBOutlet var purpleButton:UIButton!
    
    
    var counter:Double!
    var timer = Timer()
    
    var isTestCase:Bool!
    
    //shuffles the text and color if guessed correctly
    func shuffle ()
    {
        if (rem <= 0)
        {
            performSegue(withIdentifier: "winColor", sender: self)
        }
        let wrd = (1 + arc4random_uniform(4))
        let clr = (1 + arc4random_uniform(4))
        
        switch wrd
        {
        case 1:
            prompt.text = "Blue"
        case 2:
            prompt.text = "Red"
        case 3:
            prompt.text = "Purple"
        case 4:
            prompt.text = "Green"
        default:
            prompt.text = "Error"
        }
        switch clr
        {
        case 1:
            prompt.textColor = UIColor.blue
        case 2:
            prompt.textColor = UIColor.red
        case 3:
            prompt.textColor = UIColor.purple
        case 4:
            prompt.textColor = UIColor.green
        default:
            prompt.textColor = UIColor.black
        }
        rem = (rem - 1)
    }
    
    //checks if the correct button was pressed, if so shuffle to a new one
    @IBAction func bluePressed(_ sender: Any)
    {
        if (prompt.text! == "Blue")
        {
            shuffle()
        }
    }
    
    //checks if the correct button was pressed, if so shuffle to a new one
    @IBAction func redPressed(_ sender: Any)
    {
        if (prompt.text! == "Red")
        {
            shuffle()
        }
        
    }
    
    //checks if the correct button was pressed, if so shuffle to a new one
    @IBAction func purlplePressed(_ sender: Any)
    {
        if (prompt.text! == "Purple")
        {
            shuffle()
        }
    }
    
    //checks if the correct button was pressed, if so shuffle to a new one
    @IBAction func greenPressed(_ sender: Any)
    {
        if (prompt.text! == "Green")
        {
            shuffle()
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        rem = 15
        // sets timer
        timer = Timer.scheduledTimer(timeInterval: 0.1, target: self,
                                     selector: #selector(UpdateTime),
                                     userInfo: nil, repeats: true)
        shuffle()
    }
    
    @objc func UpdateTime()
    {
        counter = counter + 0.1
        titleLabel.text = "Time: " + String(format: "%.1f", counter)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let mathController = segue.destination as? MathGameController
        {
            mathController.counter = counter
            mathController.isTestCase = isTestCase
        }
    }
}
