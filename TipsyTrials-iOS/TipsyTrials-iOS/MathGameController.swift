//
//  MathGameController.swift
//  TipsyTrials-iOS
//
//  Created by Cori Calabi on 5/8/18.
//  Copyright © 2018 Tipsy Trials. All rights reserved.
//

import UIKit

class MathGameController: UIViewController
{
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet weak var MathInCorrect: UILabel!
    @IBOutlet weak var MathQuestionSpace: UILabel!
    @IBOutlet weak var MathAnswerSpace: UILabel!
    @IBOutlet weak var MathScoreSpace: UILabel!
    @IBOutlet weak var submitButton:UIButton!
    
    var operations = ["+", "-", "*", "/"]
    var numberOne: Int = 0
    var numberTwo: Int = 0
    var numberOp: Int = 0
    var correctCount: Int = 0
    var userAnswer = ""
    var trueAnswer: Int = 0
    var counter:Double!
    var timer = Timer()
    var isTestCase:Bool?
    
    override func viewDidLoad()
    {
        timer = Timer.scheduledTimer(timeInterval: 0.1, target: self,
                                     selector: #selector(UpdateTime),
                                     userInfo: nil, repeats: true)
        numberOne = 0
        numberTwo = 0
        numberOp = 0
        correctCount = 0
        userAnswer = ""
        trueAnswer = 0
        self.loadQuestion()
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func clickNumber(_ sender: UIButton)
    {
        let click = String(sender.tag - 1)
        userAnswer = userAnswer + click
        MathAnswerSpace.text = userAnswer
    }
    
    // Check if user answer is correct based on given numbers and operation
    @IBAction func checkAnswer(_ sender: UIButton)
    {
        /**
         * CHEAT CODE: REMOVE THIS IF YOU WANT
         */
        if(userAnswer == "00000")
        {
            performSegue(withIdentifier: "mazeSegue", sender: self)
        }
        /**
         * ^^^ CHEAT CODE ABOVE ^^^
         */
        
        if (numberOp == 0)
        {
            trueAnswer = numberOne + numberTwo
        }
        else if (numberOp == 1)
        {
            trueAnswer = numberOne - numberTwo
        }
        else if (numberOp == 2)
        {
            trueAnswer = numberOne * numberTwo
        }
        else if (numberOp == 3)
        {
            trueAnswer = numberOne / numberTwo
        }
        let intAns = Int(userAnswer)
        if (intAns == trueAnswer)
        {
            MathInCorrect.text = "Correct! Next Question:"
            MathAnswerSpace.text = ""
            userAnswer = ""
            correctCount = correctCount + 1
            loadQuestion()
        }
        else
        {
            MathInCorrect.text = "Sorry, Incorrect. Try Again"
            MathAnswerSpace.text = ""
            userAnswer = ""
        }
        let scoreText = "Score: " + String(correctCount)
        MathScoreSpace.text = scoreText
    }
    
    // Clear user's answer field
    @IBAction func clearAnswer(_ sender: UIButton)
    {
        MathAnswerSpace.text = ""
        userAnswer = ""
    }
    
    // Create a new question and show to the user:
    // Randomly select two numbers (0-12) and a math operation (+, -, *, /)
    // If the user has correctly answered 5 questions, load next game
    func loadQuestion()
    {
        if (correctCount < 5)
        {
            userAnswer = ""
            numberOne = Int(arc4random_uniform(13))
            numberTwo = Int(arc4random_uniform(13))
            numberOp = Int(arc4random_uniform(4))  // Random operation, +,-,*,/ 0,1,2,3
            // if doing division, check for division by zero and non-integer answers
            if (numberOp == 3)
            {
                if (numberTwo == 0)
                {
                    numberTwo = Int(arc4random_uniform(12) + 1)
                }
                while (numberOne % numberTwo != 0)
                {
                    numberOne = Int(arc4random_uniform(13))
                    numberTwo = Int(arc4random_uniform(12) + 1)
                }
            }
            // if doing subtraction, check for negative answers
            else if (numberOp == 1)
            {
                while ((numberOne - numberTwo) < 0)
                {
                    numberOne = Int(arc4random_uniform(13))
                }
            }
            // Put the question in the view
            let str1: String = String(numberOne)
            let str2: String = String(numberTwo)
            let strOp: String = operations[numberOp]
            MathQuestionSpace.text = str1 + " " + strOp + " " + str2 + " ="
        }
        else
        {   // if have answered correctly 5 questions, move on
            MathQuestionSpace.text = "Math game complete!"
            MathAnswerSpace.text = ""
            MathInCorrect.text = ""
            timer.invalidate()
            performSegue(withIdentifier: "mazeSegue", sender: self)
        }
        return;
    }
    
    // Update timer
    @objc func UpdateTime()
    {
        counter = counter + 0.1
        titleLabel.text = "Time: " + String(format: "%.1f", counter)
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if let mazeController = segue.destination as? MazeViewController
        {
            mazeController.counter = counter
            mazeController.isTestCase = isTestCase
        }
    }

}
